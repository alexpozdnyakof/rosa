import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'rosa-icon-document',
  template: '<svg width="21" height="23" viewBox="0 0 21 23" fill="none" xmlns="http://www.w3.org/2000/svg"><rect width="21" height="23" fill="#000" fill-opacity="0"></rect><path fill-rule="evenodd" clip-rule="evenodd" d="M19 8v13H5v-1H3v2c0 .552.45 1 1 1h16c.55 0 1-.448 1-1V6l-6-6H4c-.55 0-1 .448-1 1v9h2V2h8v4a2 2 0 0 0 2 2h4zm-4-5.172L18.172 6H15V2.828zM1 11c-.55 0-1 .448-1 1v6c0 .552.45 1 1 1h14c.55 0 1-.448 1-1v-6c0-.552-.45-1-1-1H1z" fill="#282423"></path><path d="M1.698 12.8V17h.9v-1.38h.684c1.014 0 1.812-.324 1.812-1.41 0-1.086-.798-1.41-1.812-1.41H1.698zm2.478 1.41c0 .492-.39.618-.852.618h-.726v-1.236h.726c.462 0 .852.12.852.618zm1.998-1.41V17h1.572c1.494 0 2.202-.882 2.202-2.1s-.708-2.1-2.202-2.1H6.174zm2.856 2.1c0 .864-.45 1.308-1.284 1.308h-.672v-2.616h.672c.834 0 1.284.444 1.284 1.308zm2.077-2.1V17h.9v-1.602h1.89v-.81h-1.89v-.978h2.064v-.81h-2.964z" fill="#fff"></path></svg>',
  styles: [':host{width: 21px; height: 23px;}'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DocumentIconComponent {}

