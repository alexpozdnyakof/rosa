
import { storiesOf, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { ControlHelperComponent } from '../../common/control-helper/control-helper.component';
import { ControlErrorComponent } from '../../common/control-error/control-error.component';
import { SharedFormModule } from '../../shared/shared.module';
import { PhoneContainerComponent } from './phone-container.component';
import { RosaPhone } from '../phone.directive';


storiesOf('Phone Input', module)
  .addDecorator(
    moduleMetadata({
      declarations: [
          RosaPhone,
          PhoneContainerComponent,
          ControlHelperComponent,
          ControlErrorComponent
      ],
      imports: [
          CommonModule,
          SharedFormModule
        ],
    })
  )
  .add('default', () => {
    return {
      template: `
      <section class="rosa_form-group">
        <rosa-phone-container>
            <label for="phone">Мобильный телефон:</label>
            <input type="text" id="phone" tabindex="0" rosaPhone mask="000 000 00 00"/>
        </rosa-phone-container>
      </section>
      `,
    };
  })