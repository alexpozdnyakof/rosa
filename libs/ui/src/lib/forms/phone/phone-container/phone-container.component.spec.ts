import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PhoneContainerComponent } from './phone-container.component';
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TemplateDrivenSpec, ReactiveSpec } from '../../../tests/container';
import { RosaPhone } from '../phone.directive';

@Component({
  template: `
    <rosa-phone-container>
        <label for="phone">Мобильный телефон:</label>
        <input type="text" id="phone" tabindex="0" rosaPhone mask="000 000 00 00"/>
        <rosa-control-helper>Helper message</rosa-control-helper>
        <rosa-control-error>Error message</rosa-control-error>
    </rosa-phone-container>
  `,
})
class SimpleTest {
  disabled = false;
  model = '';
}

@Component({
  template: `
  <form [formGroup]="form">
    <rosa-phone-container>
      <label for="phone">Мобильный телефон:</label>
      <input type="text" id="phone" formControlName="model" tabindex="0" rosaPhone mask="000 000 00 00"/>
      <rosa-control-helper>Helper message</rosa-control-helper>
      <rosa-control-error>Error message</rosa-control-error>
    </rosa-phone-container>
  </form>`,
})
class ReactiveTest {
  disabled = false;
  form = new FormGroup({
    model: new FormControl({ value: '', disabled: this.disabled }, Validators.required),
  });
}

describe('RosaInputContainer', () => {
  TemplateDrivenSpec(PhoneContainerComponent, RosaPhone, SimpleTest, '[rosaPhone]');
  ReactiveSpec(PhoneContainerComponent, RosaPhone, ReactiveTest, '[rosaPhone]');
});

describe('PhoneContainerComponent', () => {
  let component: PhoneContainerComponent;
  let fixture: ComponentFixture<PhoneContainerComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhoneContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhoneContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
