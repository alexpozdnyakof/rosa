import { Component, OnInit, AfterViewInit, ContentChild, OnDestroy } from '@angular/core';
import { NgControl } from '@angular/forms';
import { debounceTime, tap } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'rosa-phone-container',
  templateUrl: './phone-container.component.html',
  styleUrls: ['./phone-container.component.css']
})
export class PhoneContainerComponent implements OnInit, AfterViewInit, OnDestroy {
  public valid = true;
  @ContentChild(NgControl, { static: false })
  control: NgControl;
  ngAfterViewInit() {
    if(this.control){
    this.control.valueChanges.pipe(
      untilDestroyed(this),
      debounceTime(300),
      tap(data => {this.valid = this.control.valid})
      ).subscribe()
    }
  }
  constructor() { }

  ngOnInit() {}
  ngOnDestroy() {}

}
