import { RosaPhone } from './phone.directive';
import { ControlStandaloneSpec } from '../../tests/control';
import { Component } from '@angular/core';
import { ComponentFixture, TestModuleMetadata, TestBed } from '@angular/core/testing';
import { getElement } from '../../testing/get-element';
import { doClassesMatch } from '../../testing/do-classes-match';

@Component({
  template: `
       <input type="text" rosaPhone />
    `,
})
class StandaloneUseTest {}


describe('PhoneDirective', () => {
  describe('Input Standalone', () => {
    ControlStandaloneSpec(StandaloneUseTest);
  })
  describe('PhoneDirective', () => {
    let fixture: ComponentFixture<any>;
    beforeEach(() => {
      const testModuleMetadata: TestModuleMetadata = {
        declarations: [RosaPhone, StandaloneUseTest]
      };
      fixture = TestBed.configureTestingModule(testModuleMetadata).createComponent(StandaloneUseTest);
      fixture.detectChanges();
    })

  describe('must add class rosa-phone', () => {
    let inputElement = null;
    beforeEach(() => {
      const defaultTrueElementIndex = 0;
      inputElement = getElement(fixture, defaultTrueElementIndex);
    });
    it('should default class without params', () => {
      expect(doClassesMatch(inputElement.classList, ['rosa-phone'])).toBeTruthy();
    });
    })
  })
});
