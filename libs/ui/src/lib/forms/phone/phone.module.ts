import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RosaPhone } from './phone.directive';
import { PhoneContainerComponent } from './phone-container/phone-container.component';
import { SharedFormModule } from './../shared/shared.module';



@NgModule({
  declarations: [RosaPhone, PhoneContainerComponent],
  imports: [CommonModule, SharedFormModule],
  exports: [RosaPhone, PhoneContainerComponent],
})
export class PhoneModule { }
