import { Directive, Renderer2, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: 'input[rosaPhone]'
})
export class RosaPhone implements OnInit {

  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) { }

  public ngOnInit(){
    this.renderer.addClass(this.el.nativeElement, 'rosa-phone');
  }

}
