import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RosaCheckbox } from './checkbox.directive';
import { CheckboxContainerComponent } from './checkbox-container/checkbox-container.component';
import { RosaCheckboxLabel } from './checkbox-label.directive';



@NgModule({
  declarations: [
    RosaCheckbox,
    CheckboxContainerComponent,
    RosaCheckboxLabel
  ],
  imports: [
    CommonModule
  ],
  exports: [
    RosaCheckbox,
    CheckboxContainerComponent,
    RosaCheckboxLabel
  ],
  entryComponents: [
    CheckboxContainerComponent
  ]
})
export class CheckboxModule { }
