import { Directive, OnInit, OnChanges, Input, ElementRef, Renderer2, SimpleChanges } from '@angular/core';

@Directive({
  selector: 'label[rosaCheckboxLabel]'
})
export class RosaCheckboxLabel implements OnInit, OnChanges {
  @Input() value: string;
  constructor(
    private host: ElementRef,
    private renderer: Renderer2
  ) { }
  ngOnChanges(changes: SimpleChanges){
    this.changeView();
  }
  ngOnInit(): void{
    this.changeView();
  }
  public changeView(){
    const html = `
    <div class="rosa-checkbox__icon">
      <svg width="24" height="24" viewBox="0 0 24 24" fill="none">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M21.744 5.67L10.23 18.44l-6.926-6.722 1.393-1.435 5.437 5.277L20.258 4.33l1.486 1.34z" fill="#E60028">
      </svg>
    </div>
    <div class="rosa-checkbox__caption">${this.value}</div>
  `;
    this.renderer.setProperty(this.host.nativeElement, 'innerHTML', html);
  }

}
