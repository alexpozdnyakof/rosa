import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckboxContainerComponent } from './checkbox-container.component';
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TemplateDrivenSpec, ReactiveSpec } from '../../../tests/toggle-container';
import { RosaCheckbox } from '../checkbox.directive';


@Component({
  template: `
  <rosa-checkbox-container>
    <input type="checkbox" rosaCheckbox="rosaCheckbox"  id="checkbox" name="adsFlag" checked />
    <label for="checkbox" value="Согласен на обработку персональных данных" rosaCheckboxLabel="rosaCheckboxLabel"></label>
    <rosa-control-helper>Helper message</rosa-control-helper>
    <rosa-control-error>Error message</rosa-control-error>
  </rosa-checkbox-container>
  `,
})
class SimpleTest {
  disabled = false;
  model = '';
}

@Component({
  template: `
  <form [formGroup]="form">
    <rosa-checkbox-container [theme]="'dark'">
      <input type="checkbox" rosaCheckbox="rosaCheckbox"  id="checkbox" formControlName="model" name="model" checked />
      <label for="checkbox" value="Согласен на обработку персональных данных" rosaCheckboxLabel="rosaCheckboxLabel"></label>
      <rosa-control-helper>Helper message</rosa-control-helper>
      <rosa-control-error>Error message</rosa-control-error>
    </rosa-checkbox-container>
  </form>`,
})
class ReactiveTest {
  disabled = false;
  form = new FormGroup({
    model: new FormControl({ value: '', disabled: this.disabled }, Validators.required),
  });
}

describe('RosaCheckboxContainer', () => {
  TemplateDrivenSpec(CheckboxContainerComponent, RosaCheckbox, SimpleTest, '[rosaCheckbox]');
  ReactiveSpec(CheckboxContainerComponent, RosaCheckbox, ReactiveTest, '[rosaCheckbox]');
});



