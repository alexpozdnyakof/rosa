import { storiesOf, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { ControlHelperComponent } from '../../common/control-helper/control-helper.component';
import { ControlErrorComponent } from '../../common/control-error/control-error.component';
import { SharedFormModule } from '../../shared/shared.module';
import { CheckboxContainerComponent } from './checkbox-container.component';
import { RosaCheckboxLabel } from '../checkbox-label.directive';
import { RosaCheckbox } from '../checkbox.directive';

storiesOf('Checkbox', module)
  .addDecorator(
    moduleMetadata({
      declarations: [
        RosaCheckboxLabel,
        RosaCheckbox,
        CheckboxContainerComponent,
        ControlHelperComponent,
        ControlErrorComponent
      ],
      imports: [CommonModule, SharedFormModule]
    })
  )
  .add('default', () => {
    return {
      template: `
      <section class="rosa_form-group rosa-checkbox_group">
        <label class="checkbox-form-group__label">Примите соглашения чтобы продолжить</label>
        <rosa-checkbox-container>
                <input type="checkbox" rosaCheckbox="rosaCheckbox"  id="checkbox" name="adsFlag" checked />
                <label for="checkbox" value="Согласен на обработку персональных данных" rosaCheckboxLabel="rosaCheckboxLabel"></label>
        </rosa-checkbox-container>
        <rosa-checkbox-container>
            <input type="checkbox" rosaCheckbox="rosaCheckbox"  id="checkbox2" name="adsFlag" />
            <label for="checkbox2" value="Согласен на получение рекламы" rosaCheckboxLabel="rosaCheckboxLabel"></label>
        </rosa-checkbox-container>
      </section>
      `
    };
  })
  .add('disabled', () => {
    return {
      template: `
      <section class="rosa_form-group rosa-checkbox_group">
        <label class="checkbox-form-group__label">Примите соглашения чтобы продолжить</label>
        <rosa-checkbox-container>
                <input type="checkbox" rosaCheckbox="rosaCheckbox"  id="checkbox" name="adsFlag" checked disabled/>
                <label for="checkbox" value="Согласен на обработку персональных данных" rosaCheckboxLabel="rosaCheckboxLabel"></label>
        </rosa-checkbox-container>
        <rosa-checkbox-container>
            <input type="checkbox" rosaCheckbox="rosaCheckbox"  id="checkbox2" name="adsFlag" disabled/>
            <label for="checkbox2" value="Согласен на получение рекламы" rosaCheckboxLabel="rosaCheckboxLabel"></label>
        </rosa-checkbox-container>
      </section>
      `
    };
  })
  .add('gray', () => {
    return {
      template: `
      <section class="rosa_form-group rosa_form-group__gray rosa-checkbox_group">
        <label class="checkbox-form-group__label">Примите соглашения чтобы продолжить</label>
        <rosa-checkbox-container [theme]="'gray'">
                <input type="checkbox" rosaCheckbox="rosaCheckbox"  id="checkbox" name="adsFlag" checked />
                <label for="checkbox" value="Согласен на обработку персональных данных" rosaCheckboxLabel="rosaCheckboxLabel"></label>
        </rosa-checkbox-container>
        <rosa-checkbox-container [theme]="'gray'">
            <input type="checkbox" rosaCheckbox="rosaCheckbox"  id="checkbox2" name="adsFlag" />
            <label for="checkbox2" value="Согласен на получение рекламы" rosaCheckboxLabel="rosaCheckboxLabel"></label>
        </rosa-checkbox-container>
      </section>
      `
    };
  })
  .add('dark', () => {
    return {
      template: `
      <section class="rosa_form-group rosa_form-group__dark rosa-checkbox_group theme_dark">
        <label class="checkbox-form-group__label">Выберите способ получения карты</label>
        <rosa-checkbox-container [theme]="'dark'">
                <input type="checkbox" rosaCheckbox="rosaCheckbox"  id="checkbox" name="adsFlag" checked />
                <label for="checkbox" value="Согласен на обработку персональных данных" rosaCheckboxLabel="rosaCheckboxLabel"></label>
        </rosa-checkbox-container>
        <rosa-checkbox-container [theme]="'dark'">
            <input type="checkbox" rosaCheckbox="rosaCheckbox"  id="checkbox2" name="adsFlag"/>
            <label for="checkbox2" value="Согласен на получение рекламы" rosaCheckboxLabel="rosaCheckboxLabel"></label>
        </rosa-checkbox-container>
      </section>
      `
    };
  })
  .add('subcontent', () => {
    return {
      template: `
      <section class="rosa_form-group rosa_form-group__gray rosa-checkbox_group">
        <label class="checkbox-form-group__label">Примите соглашения чтобы продолжить</label>
        <rosa-checkbox-container [theme]="'gray'">
                <input type="checkbox" rosaCheckbox="rosaCheckbox"  id="checkbox" name="adsFlag" checked />
                <label for="checkbox" value="Согласен на обработку персональных данных" rosaCheckboxLabel="rosaCheckboxLabel"></label>
                <span style="font-size:9px; font-weight: 500; letter-spacing: 1.1px; text-transform: uppercase;">дополнительный текст</span>
        </rosa-checkbox-container>
        <rosa-checkbox-container [theme]="'gray'">
            <input type="checkbox" rosaCheckbox="rosaCheckbox"  id="checkbox2" name="adsFlag" />
            <label for="checkbox2" value="Согласен на получение рекламы" rosaCheckboxLabel="rosaCheckboxLabel"></label>
            <span style="font-size:9px; font-weight: 500; letter-spacing: 1.1px; text-transform: uppercase;">дополнительный текст</span>
        </rosa-checkbox-container>
      </section>
      `
    };
  });
