import { Component, OnInit, Input, ViewChild, ElementRef, ContentChild, Renderer2, AfterViewInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { NgControl } from '@angular/forms';
import { debounceTime, tap } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'rosa-checkbox-container',
  templateUrl: './checkbox-container.component.html',
  styleUrls: ['./checkbox-container.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CheckboxContainerComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() theme: 'default' | 'dark' | 'gray' = 'default';
  @ViewChild('themeContainer', {static: true})
  themeContainer: ElementRef
  public valid = true;
  @ContentChild(NgControl, { static: true })
  control: NgControl;
  @ContentChild('label', { static: true })
  label: HTMLLabelElement;
  constructor(private renderer: Renderer2) { }
  ngAfterViewInit() {
    if(this.control){
      this.control.statusChanges.pipe(
        debounceTime(300),
        untilDestroyed(this),
        tap(data => this.valid = this.control.valid)
      ).subscribe()
    }
  }
  private setTheme(){
    const checkboxClass = `theme_${this.theme}`;
    this.renderer.addClass(this.themeContainer.nativeElement, checkboxClass);
  }
  ngOnInit(){ this.setTheme() }
  ngOnDestroy(){}
}
