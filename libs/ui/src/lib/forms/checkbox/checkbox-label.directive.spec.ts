import { RosaCheckboxLabel } from './checkbox-label.directive';
import { Component } from '@angular/core';
import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed
} from '@angular/core/testing';
import { getElement } from '../../testing/get-element';
import { doClassesMatch } from '../../testing/do-classes-match';

@Component({
  template: `
    <label
      for="checkbox2"
      value="Value"
      rosaCheckboxLabel="rosaCheckboxLabel"
    ></label>
  `
})
class TestComponent {}

describe('Checkbox Label Directive', () => {
  let fixture: ComponentFixture<any>;
  beforeEach(() => {
    const testModuleMetadata: TestModuleMetadata = {
      declarations: [RosaCheckboxLabel, TestComponent]
    };
    fixture = TestBed.configureTestingModule(
      testModuleMetadata
    ).createComponent(TestComponent);
    fixture.detectChanges();
  });
  describe('Checkbox label', () => {
    let checkboxLabelElement = null;
    beforeEach(() => {
      const defaultTrueElementIndex = 0;
      checkboxLabelElement = getElement(fixture, defaultTrueElementIndex);
    });
    it('should have icon', () => {
      const icon = checkboxLabelElement.querySelector('.rosa-checkbox__icon');
      expect(icon).toBeTruthy();
    });
    it('should have caption', () => {
      const caption = checkboxLabelElement.querySelector(
        '.rosa-checkbox__caption'
      );
      expect(caption).toBeTruthy();
    });
    it('should set value', () => {
      const caption = checkboxLabelElement.querySelector(
        '.rosa-checkbox__caption'
      );
      expect(caption.innerHTML).toContain('Value');
    });
  });
});
