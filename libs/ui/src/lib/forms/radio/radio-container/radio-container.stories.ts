import { storiesOf, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { ControlHelperComponent } from '../../common/control-helper/control-helper.component';
import { ControlErrorComponent } from '../../common/control-error/control-error.component';
import { SharedFormModule } from '../../shared/shared.module';
import { RosaRadio } from '../radio.directive';
import { RadioContainerComponent } from './radio-container.component';

storiesOf('Radio', module)
  .addDecorator(
    moduleMetadata({
      declarations: [
        RosaRadio,
        RadioContainerComponent,
        ControlHelperComponent,
        ControlErrorComponent
      ],
      imports: [CommonModule, SharedFormModule]
    })
  )
  .add('default', () => {
    return {
      template: `
      <section class="rosa_form-group  ">
        <label class="radio-form-group__label">Примите соглашения чтобы продолжить</label>
        <div class="radio-group_column">
            <rosa-radio-container><input type="radio" rosaRadio="rosaRadio" id="branch"  name="method" value="Branch" checked="checked" /><label for="branch">Получить в отделении</label></rosa-radio-container>
            <rosa-radio-container><input type="radio" rosaRadio="rosaRadio" id="courier" name="method" value="Courier" /><label for="courier">Доставить курьером</label></rosa-radio-container>
        </div>
      </section>
      `
    };
  })
  .add('disabled', () => {
    return {
      template: `
      <section class="rosa_form-group  ">
        <label class="radio-form-group__label">Примите соглашения чтобы продолжить</label>
        <div class="radio-group_column">
            <rosa-radio-container><input type="radio" rosaRadio="rosaRadio" id="branch"  name="method" value="Branch" disabled checked="checked" /><label for="branch">Получить в отделении</label></rosa-radio-container>
            <rosa-radio-container><input type="radio" rosaRadio="rosaRadio" id="courier" name="method" value="Courier" disabled /><label for="courier">Доставить курьером</label></rosa-radio-container>
        </div>
      </section>
      `
    };
  })
  .add('grey', () => {
    return {
      template: `
      <section class="rosa_form-group rosa_form-group__gray theme_gray ">
        <label class="radio-form-group__label">Примите соглашения чтобы продолжить</label>
        <div class="radio-group_column">
            <rosa-radio-container [theme]="'gray'"><input type="radio" rosaRadio="rosaRadio" id="branch"  name="method" value="Branch" checked="checked" /><label for="branch">Получить в отделении</label></rosa-radio-container>
            <rosa-radio-container [theme]="'gray'"><input type="radio" rosaRadio="rosaRadio" id="courier" name="method" value="Courier" /><label for="courier">Доставить курьером</label></rosa-radio-container>
        </div>
      </section>
      `
    };
  })
  .add('dark', () => {
    return {
      template: `
      <section class="rosa_form-group rosa_form-group__dark theme_dark">
        <label class="radio-form-group__label">Примите соглашения чтобы продолжить</label>
        <div class="radio-group_column">
            <rosa-radio-container [theme]="'dark'"><input type="radio" rosaRadio="rosaRadio" id="branch"  name="method" value="Branch" checked="checked" /><label for="branch">Получить в отделении</label></rosa-radio-container>
            <rosa-radio-container [theme]="'dark'"><input type="radio" rosaRadio="rosaRadio" id="courier" name="method" value="Courier" /><label for="courier">Доставить курьером</label></rosa-radio-container>
        </div>
      </section>
      `
    };
  })
  .add('subcontent', () => {
    return {
      template: `
      <section class="rosa_form-group">
        <label class="radio-form-group__label">Примите соглашения чтобы продолжить</label>
        <div class="radio-group_column">
            <rosa-radio-container>
            <input type="radio" rosaRadio="rosaRadio" id="branch"  name="method" value="Branch" checked="checked" /><label for="branch">Получить в отделении</label>
            <span style="font-size:9px; font-weight: 500; letter-spacing: 1.1px; text-transform: uppercase;">дополнительный текст</span>
            </rosa-radio-container>
            <rosa-radio-container>
            <input type="radio" rosaRadio="rosaRadio" id="courier" name="method" value="Courier" /><label for="courier">Доставить курьером</label>
            <span style="font-size:9px; font-weight: 500; letter-spacing: 1.1px; text-transform: uppercase;">дополнительный текст</span>
            </rosa-radio-container>
        </div>
      </section>
      `
    };
  });
