import { Component, OnInit, ViewEncapsulation, AfterViewInit, OnDestroy, ContentChild, Input, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { NgControl } from '@angular/forms';
import { debounceTime, tap } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'rosa-radio-container',
  templateUrl: './radio-container.component.html',
  styleUrls: ['./radio-container.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RadioContainerComponent implements  OnInit, AfterViewInit, OnDestroy {
  public valid = true;
  @Input() theme: 'default' | 'dark' | 'gray' = 'default';
  @ContentChild(NgControl, { static: true }) control: NgControl;
  @ContentChild('label', { static: true }) label;
  @ViewChild('themeContainer', {static: true})
  themeContainer: ElementRef
  constructor(private renderer: Renderer2) { }

  ngAfterViewInit() {
    if(this.control){
      this.control.statusChanges.pipe(
        debounceTime(300),
        untilDestroyed(this),
        tap(data => this.valid = this.control.valid)
      ).subscribe()
    }
  }
  ngOnInit() {
    this.setTheme()
  }
  ngOnDestroy() {}
  private setTheme(){
    const checkboxClass = `theme_${this.theme}`;
    this.renderer.addClass(this.themeContainer.nativeElement, checkboxClass);
  }
}
