
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RadioContainerComponent } from './radio-container.component';
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TemplateDrivenSpec, ReactiveSpec } from '../../../tests/toggle-container';
import { RosaRadio } from '../radio.directive';


@Component({
  template: `
  <rosa-radio-container>
    <input type="radio" rosaRadio="rosaRadio" id="branch"  name="method" value="Branch" checked="checked" />
    <label for="branch">Получить в отделении</label>
    <rosa-control-helper>Helper message</rosa-control-helper>
    <rosa-control-error>Error message</rosa-control-error>
  </rosa-radio-container>
  `,
})
class SimpleTest {
  disabled = false;
  model = '';
}

@Component({
  template: `
  <form [formGroup]="form">
    <rosa-radio-container>
      <input type="radio" rosaRadio="rosaRadio" id="branch"  formControlName="model" name="model" value="Branch" checked="checked" />
      <label for="branch">Получить в отделении</label>
      <rosa-control-helper>Helper message</rosa-control-helper>
      <rosa-control-error>Error message</rosa-control-error>
    </rosa-radio-container>
  </form>`,
})
class ReactiveTest {
  disabled = false;
  form = new FormGroup({
    model: new FormControl({ value: '', disabled: this.disabled }, Validators.required),
  });
}

describe('RosaRadioContainer', () => {
  TemplateDrivenSpec(RadioContainerComponent, RosaRadio, SimpleTest, '[rosaRadio]');
  ReactiveSpec(RadioContainerComponent, RosaRadio, ReactiveTest, '[rosaRadio]');
});



