import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RosaRadio } from './radio.directive';
import { RadioContainerComponent } from './radio-container/radio-container.component';



@NgModule({
  declarations: [
    RosaRadio,
    RadioContainerComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    RosaRadio,
    RadioContainerComponent
  ],
})
export class RadioModule { }
