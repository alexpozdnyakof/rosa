import { Component, OnInit, OnDestroy, ContentChild, AfterViewInit, ContentChildren, QueryList, ViewEncapsulation } from '@angular/core';
import { NgControl } from '@angular/forms';
import { Subject, Observable, merge } from 'rxjs';
import { debounceTime, switchMap } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { SelectComponent } from '../select/select.component';

@Component({
  selector: 'rosa-select-container',
  templateUrl: './select-container.component.html',
  styleUrls: ['./select-container.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class SelectContainerComponent implements OnInit, AfterViewInit, OnDestroy {
public controlValid = true;
public isOpen = false;
@ContentChild(NgControl, { static: true })
control: NgControl;

@ContentChild(SelectComponent, { static: true })
selectControl: SelectComponent;




private _controlChanges: Subject<NgControl> = new Subject<NgControl>();
get controlChanges(): Observable<NgControl> {
  return this
  ._controlChanges.asObservable();
}
setControl(control: NgControl) {
  this._controlChanges.next(control);
}

constructor() { }
ngAfterViewInit() {
  if(this.selectControl){
    this.selectControl.isOpen$.pipe(
      untilDestroyed(this)
    ).subscribe(
      data => {
        this.isOpen = data
      }
    )
  }
  if(this.control){
    this.controlChanges.pipe(
      untilDestroyed(this)
      ).subscribe(
        data => {
          this.isControlValid(this.control.valid);
        }
      )
  }
}
ngOnInit() {

}
ngOnDestroy() {}



public isControlValid(isValid: boolean){
  this.controlValid = isValid;
}
public controlClass(){
  return 'class';
}

}
