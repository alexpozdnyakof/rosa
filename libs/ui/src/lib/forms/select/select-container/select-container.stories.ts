
import { storiesOf, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { ControlHelperComponent } from '../../common/control-helper/control-helper.component';
import { ControlErrorComponent } from '../../common/control-error/control-error.component';
import { SharedFormModule } from '../../shared/shared.module';
import { SelectContainerComponent } from './select-container.component';
import { SelectDirective } from '../select.directive';
import { SelectComponent } from '../select/select.component';
import { OptionComponent } from '../../options/option/option.component';
import { IconDropdownComponent } from '../icon-dropdown/icon-dropdown.component';

export const options = [
    { id: 1, label: 'One' },
    { id: 2, label: 'Two' },
    { id: 3, label: 'Three' },
    { id: 4, label: 'Fourth' },
    { id: 5, label: 'Five' },
  ];
const props = {
    options: options,
};
storiesOf('Select', module)
  .addDecorator(
    moduleMetadata({
      declarations: [
          SelectDirective,
          SelectContainerComponent,
          SelectComponent,
          ControlHelperComponent,
          ControlErrorComponent,
          OptionComponent,
          IconDropdownComponent
      ],
      imports: [
          CommonModule,
          SharedFormModule
        ],
    })
  )
  .add('default', () => {
    return {
      template: `
      <section class="rosa_form-group  ">
        <rosa-select-container><label>Тип работы</label>
            <rosa-select>
                <rosa-option *ngFor="let option of options" [value]="option.label">{{ option.label }}</rosa-option>
            </rosa-select>
            <rosa-control-helper>Начните вводить название организации</rosa-control-helper>
            <rosa-control-error>Выберите из списка</rosa-control-error>
        </rosa-select-container>
      </section>
      `,
      props,
    };
  })
