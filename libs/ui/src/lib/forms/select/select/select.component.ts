import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  AfterViewInit,
  ContentChildren,
  ViewChild,
  Renderer2,
  forwardRef,
  ElementRef,

} from '@angular/core';
import { switchMap, takeUntil, withLatestFrom, map } from 'rxjs/operators';
import { of, BehaviorSubject, combineLatest, merge, fromEvent } from 'rxjs';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { OptionComponent } from '../../options/option/option.component';
export const CUSTOM_SELECT_VALUE_ACCESSOR : any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => SelectComponent),
  multi: true,
};
@Component({
  selector: 'rosa-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.css'],
  providers: [CUSTOM_SELECT_VALUE_ACCESSOR]
})
export class SelectComponent implements OnInit, AfterViewInit, ControlValueAccessor, OnDestroy {

  @ContentChildren(OptionComponent)
  options: QueryList<OptionComponent>;
  @ViewChild('selectValue', {static: false, read: ElementRef})
  public selectValue;
  @Input()
  public placeholder = `Выберите из списка`;

  public value: string;
  private _isOpen = false;
  public isOpen$;

  constructor(
    private host: ElementRef,
  ) {}
  ngAfterViewInit(): void {
    this.optionsClick()
    this.overlayClickOutside().subscribe(
      data => {
        if(!this.host.nativeElement.contains(data)){
          this.close();
        }
      }
    )
  }
  ngOnInit() {
    this.isOpen$ = new BehaviorSubject<Boolean>(this._isOpen);
  }
  ngOnDestroy() {}
  onChange: any = () => {};
  onTouched: any = () => {};
  optionsClick() {
    const clicks$ = this.options.map(option => option.click$);
    const mergeClicks = merge(...clicks$);
    mergeClicks.subscribe(
      data => {
        this.writeValue(data)
      }
    )
  }

  public writeValue( value : any ) : void {
    if(!value || typeof value !== 'string' ){
      return;
    }

    this.value = value;
    //const div = this.selectValue.nativeElement;
    //this.renderer.setProperty(div, 'textContent', value);
    this.onChange(value);
    if(this._isOpen){this.close()}
  }
  registerOnTouched( fn : any ) : void {
    this.onTouched = fn;
  }
  registerOnChange( fn : any ) : void {
    this.onChange = fn;
  }

  public toggleDropdown(){
    this.isOpen$.next(!this._isOpen);
    this.onTouched();
    return this._isOpen = !this._isOpen;
  }
  public close(){
    this.isOpen$.next(false);
    return this._isOpen = false;
  }
  isOpen(){
    return this._isOpen ? 'select__open' : '';
  }
  overlayClickOutside() {
    return fromEvent<MouseEvent>(document, 'click')
    .pipe(
      untilDestroyed(this),
      map(event => event.target as HTMLElement)
    )
  }
}
