import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectComponent } from './select/select.component';
import { SelectContainerComponent } from './select-container/select-container.component';
import { SelectDirective } from './select.directive';
import { IconDropdownComponent } from './icon-dropdown/icon-dropdown.component';



@NgModule({
  declarations: [
    SelectComponent,
    SelectContainerComponent,
    SelectDirective,
    IconDropdownComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SelectComponent,
    SelectContainerComponent,
    SelectDirective,
    IconDropdownComponent
  ],
})
export class SelectModule { }
