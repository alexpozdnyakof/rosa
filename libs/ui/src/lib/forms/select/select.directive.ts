import { Directive, HostBinding, ElementRef, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: 'select[rosaSelect]'
})
export class SelectDirective implements OnInit {

  @HostBinding('class.rosa-input')
  get medium() {
    return true;
  }
  constructor(
    private host: ElementRef,
    private renderer: Renderer2
    ) { }

  ngOnInit(): void {
    console.log(this.host.nativeElement);
    const selectReplicant = this.renderer.createElement('div');
    this.renderer.addClass(selectReplicant, 'rosa-select');
    this.renderer.insertBefore(this.host.nativeElement, selectReplicant, this.host.nativeElement);
    this.renderer.setStyle(
      this.host.nativeElement,
      'display',
      'none' )


  }
}
