import { NgModule } from '@angular/core';

import { RosaInputModule } from './input/input.module';
import { CommonFormModule } from './common/common.module';
import { SharedFormModule } from './shared/shared.module';
import { PhoneModule } from './phone/phone.module';
import { CheckboxModule } from './checkbox/checkbox.module';
import { SelectModule } from './select/select.module';


@NgModule({
  declarations: [

  ],
  imports: [
  SharedFormModule,
    RosaInputModule,
    CommonFormModule,
    PhoneModule,
    CheckboxModule,
    SelectModule
  ],
  exports: [
    RosaInputModule,
    PhoneModule,
    CheckboxModule,
    CommonFormModule,
    SelectModule
  ]
})
export class RosaFormModule {}
