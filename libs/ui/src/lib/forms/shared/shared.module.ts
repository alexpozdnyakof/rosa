import { NgModule } from '@angular/core';
import { NgxMaskModule } from 'ngx-mask';
import { OverlayModule } from '@angular/cdk/overlay';

@NgModule({
  imports: [NgxMaskModule.forRoot(), OverlayModule],
  declarations: [],
  exports: [NgxMaskModule, OverlayModule]
})
export class SharedFormModule {}
