import { Directive, Input, Renderer2, ElementRef, OnInit } from '@angular/core';

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: 'input[rosaInput], textarea[rosaInput]'
})
export class RosaInput implements OnInit {
  private _type: 'default' | 'large' | 'numeric';

  @Input('rosaInput')
  set setType(value: 'default' | 'large' | 'numeric') {
    this._type = value || `default`;
  }
  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) { }

  ngOnInit(){
    const type = `rosa-input_${this._type}`;
    this.renderer.addClass(this.el.nativeElement, 'rosa-input');
    this.renderer.addClass(this.el.nativeElement, type);
  }
}