import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RosaInput } from './input.directive';
import { InputContainerComponent } from './input-container/input-container.component';
import { CommonFormModule } from './../common/common.module';
import { SharedFormModule } from './../shared/shared.module';


@NgModule({
  declarations: [
    RosaInput,
    InputContainerComponent
  ],
  imports: [
    CommonModule,
    CommonFormModule,
    SharedFormModule
  ],
  exports: [
    RosaInput
  ]
})
export class RosaInputModule { }
