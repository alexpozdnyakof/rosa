import { RosaInput } from './input.directive';
import { Component } from '@angular/core';
import { ComponentFixture, TestModuleMetadata, TestBed } from '@angular/core/testing';
import { getElement } from '../../testing/get-element';
import { doClassesMatch } from '../../testing/do-classes-match';
import { ControlStandaloneSpec } from '../../tests/control';

@Component({
  template: `
  <input type="text" rosaInput>
  <input type="text" [rosaInput]="'large'">
  <input type="text" [rosaInput]="'numeric'">
  `
})
class TestComponent { }

@Component({
  template: `
       <input type="text" rosaInput />
    `,
})
class StandaloneUseTest {}

describe('Input Standalone', () => {
  ControlStandaloneSpec(StandaloneUseTest);
})

describe('InputDirective', () => {
  let fixture: ComponentFixture<any>;
  beforeEach(() => {
    const testModuleMetadata: TestModuleMetadata = {
      declarations: [RosaInput, TestComponent]
    };
    fixture = TestBed.configureTestingModule(testModuleMetadata).createComponent(TestComponent);
    fixture.detectChanges();
  });
  describe('Directive: Input without params', () => {
    let inputElement = null;
    beforeEach(() => {
      const defaultTrueElementIndex = 0;
      inputElement = getElement(fixture, defaultTrueElementIndex);
    });
    it('should default class without params', () => {
      expect(doClassesMatch(inputElement.classList, ['rosa-input_default'])).toBeTruthy();
    });
  })

  describe('Directive: Input large', () => {
    let inputElement = null;
    beforeEach(() => {
      const defaultTrueElementIndex = 1;
      inputElement = getElement(fixture, defaultTrueElementIndex);
    });
    it('should large class', () => {
      expect(doClassesMatch(inputElement.classList, ['rosa-input_large'])).toBeTruthy();
    });
  })

  describe('Directive: Input Numeric', () => {
    let inputElement = null;
    beforeEach(() => {
      const defaultTrueElementIndex = 2;
      inputElement = getElement(fixture, defaultTrueElementIndex);
    });
    it('should numeric class', () => {
      expect(doClassesMatch(inputElement.classList, ['rosa-input_numeric'])).toBeTruthy();
    });
  })
  /*
  it('should create an instance', () => {
    const directive = new InputDirective();
    expect(directive).toBeTruthy();
  });
  */
});
