
import { RosaInput } from './../input.directive';
import { storiesOf, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { InputContainerComponent } from './input-container.component';
import { ControlHelperComponent } from './../../common/control-helper/control-helper.component';
import { ControlErrorComponent } from './../../common/control-error/control-error.component';
import { SharedFormModule } from '../../shared/shared.module';


storiesOf('Input', module)
  .addDecorator(
    moduleMetadata({
      declarations: [
          RosaInput,
          InputContainerComponent,
          ControlHelperComponent,
          ControlErrorComponent
      ],
      imports: [
          CommonModule,
          SharedFormModule
        ],
    })
  )
  .add('default', () => {
    return {
      template: `
      <section class="rosa_form-group">
        <rosa-input-container>
            <label for="docEntityCode">Ваше имя</label>
            <input type="text" id="docEntityCode" tabindex="0" rosaInput />
        </rosa-input-container>
      </section>
      `,
    };
  })
  .add('large', () => {
    return {
      template: `
      <section class="rosa_form-group">
        <rosa-input-container>
            <label for="docEntityCode">Сумма кредита</label>
            <input type="text" id="docEntityCode" tabindex="0" [rosaInput]="'large'"  mask="0 000 000"/>
        </rosa-input-container>
      </section>
      `,
    };
  })
  .add('only digits', () => {
    return {
      template: `
      <section class="rosa_form-group">
        <rosa-input-container>
            <label for="docEntityCode">Номер паспорта</label>
            <input type="text" id="docEntityCode" pattern="\d*" tabindex="0" rosaInput />
        </rosa-input-container>
      </section>
      `,
    };
  })
  .add('with mask', () => {
    return {
      template: `
      <section class="rosa_form-group">
        <rosa-input-container>
            <label for="docEntityCode">Номер паспорта</label>
            <input type="text" id="docEntityCode" pattern="\d*" tabindex="0" rosaInput placeholder="0000 0000" mask="0000 0000"/>
        </rosa-input-container>
      </section>
      `,
    };
  })
  .add('readonly', () => {
    return {
      template: `
      <section class="rosa_form-group">
        <rosa-input-container>
            <label for="docEntityCode">Номер паспорта</label>
            <input type="text" [readonly]="true"  id="docEntityCode" pattern="\d*" tabindex="0" rosaInput value="0000" />
        </rosa-input-container>
      </section>
      `,
    };
  })
  .add('disabled', () => {
    return {
      template: `
      <section class="rosa_form-group">
        <rosa-input-container>
            <label for="docEntityCode">Номер паспорта</label>
            <input type="text" [disabled]="true"  id="docEntityCode" pattern="\d*" tabindex="0" rosaInput value="0000" />
        </rosa-input-container>
      </section>
      `,
    };
  })
  .add('with error', () => {
    return {
      template: `
      <section class="rosa_form-group">
        <rosa-input-container>
            <label for="docEntityCode">Ваше имя</label>
            <input type="text" id="docEntityCode" tabindex="0" rosaInput />
            <rosa-control-error>Заполните поле</rosa-control-error>
        </rosa-input-container>
      </section>
      `,
    };
  })
  .add('with message', () => {
    return {
      template: `
      <section class="rosa_form-group">
        <rosa-input-container>
            <label for="docEntityCode">Ваше имя</label>
            <input type="text" id="docEntityCode" tabindex="0" rosaInput />
            <rosa-control-helper>Введите имя как в паспорте</rosa-control-helper>
        </rosa-input-container>
      </section>
      `,
    };
  })
  .add('with placeholder', () => {
    return {
      template: `
      <section class="rosa_form-group">
        <rosa-input-container>
            <label for="docEntityCode">Ваше имя</label>
            <input type="text" id="docEntityCode" tabindex="0" rosaInput  placeholder="Константин" />
            <rosa-control-helper>Введите имя как в паспорте</rosa-control-helper>
        </rosa-input-container>
      </section>
      `,
    };
  })
