import { Component, OnInit, ContentChild, AfterViewInit, OnDestroy } from '@angular/core';
import { NgControl } from '@angular/forms';
import { Subject, Observable } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'rosa-input-container',
  templateUrl: './input-container.component.html',
  styleUrls: ['./input-container.component.css']
})
export class InputContainerComponent implements OnInit, AfterViewInit, OnDestroy {
  @ContentChild(NgControl, { static: false })
  control: NgControl;
  public valid = true;

  ngAfterViewInit() {
    if(this.control){
    this.control.statusChanges.pipe(
      debounceTime(300),
      untilDestroyed(this)
      ).subscribe(
        data => {this.valid = this.control.valid}
      )
    }
  }
  constructor() { }
  ngOnInit() {
  }
  ngOnDestroy(){}

}
