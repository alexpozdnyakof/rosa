import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputContainerComponent } from './input-container.component';
import { Component } from '@angular/core';
import { RosaInput } from '../input.directive';
import { TemplateDrivenSpec, ReactiveSpec } from '../../../tests/container';
import { FormGroup, Validators, FormControl } from '@angular/forms';


@Component({
  template: `
  <rosa-input-container>
    <label for="docEntityCode">Label field</label>
    <input type="text" id="docEntityCode" tabindex="0" rosaInput [(ngModel)]="model" name="model" placeholder="000-000" />
    <rosa-control-helper>Helper message</rosa-control-helper>
    <rosa-control-error>Error message</rosa-control-error>
  </rosa-input-container>
  `,
})
class SimpleTest {
  disabled = false;
  model = '';
}

@Component({
  template: `
  <form [formGroup]="form">
    <rosa-input-container>
      <label for="docEntityCode">Label field</label>
      <input type="text" id="docEntityCode" tabindex="0" rosaInput formControlName="model" placeholder="000-000" />
      <rosa-control-helper>Helper message</rosa-control-helper>
      <rosa-control-error>Error message</rosa-control-error>
    </rosa-input-container>
  </form>`,
})
class ReactiveTest {
  disabled = false;
  form = new FormGroup({
    model: new FormControl({ value: '', disabled: this.disabled }, Validators.required),
  });
}

describe('RosaInputContainer', () => {
  TemplateDrivenSpec(InputContainerComponent, RosaInput, SimpleTest, '[rosaInput]');
  ReactiveSpec(InputContainerComponent, RosaInput, ReactiveTest, '[rosaInput]');
});

