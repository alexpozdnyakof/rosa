import { untilDestroyed } from 'ngx-take-until-destroy';
import {
  Component,
  ContentChild,
  ContentChildren,
  QueryList,
  TemplateRef,
  ViewChild,
  AfterViewInit,
  OnDestroy
} from '@angular/core';
import { switchMap } from 'rxjs/operators';
import { merge, BehaviorSubject, Observable, combineLatest } from 'rxjs';
import { ActiveDescendantKeyManager } from '@angular/cdk/a11y';
import { AutocompleteContentDirective } from '../autocomplete-content.directive';
import { OptionComponent } from '../../options/option/option.component';

@Component({
  selector: 'rosa-autocomplete',
  template:
    '<ng-template #root><div class="autocomplete"><ng-container *ngTemplateOutlet="content.tpl"></ng-container></div></ng-template>',
  exportAs: 'rosaAutocomplete',
  styleUrls: ['./autocomplete.component.css']
})
export class AutocompleteComponent implements AfterViewInit, OnDestroy {
  @ViewChild('root', { static: false })
  rootTemplate: TemplateRef<any>;
  @ContentChild(AutocompleteContentDirective, { static: false })
  content: AutocompleteContentDirective;
  @ContentChildren(OptionComponent) options: QueryList<OptionComponent>;
  isOptions = new BehaviorSubject(false);
  public _keyBoardSelectedValue$: BehaviorSubject<any> = new BehaviorSubject(
    false
  );
  private keyManager: ActiveDescendantKeyManager<OptionComponent>;

  public onKeyDown(event) {
    if (event.keyCode === 13) {
      if (this.keyManager.activeItem) {
        this._keyBoardSelectedValue$.next(this.keyManager.activeItem.value);
      }
      return this._keyBoardSelectedValue$.next(false);
    }
    this.keyManager.onKeydown(event);
  }
  ngAfterViewInit() {
    this.keyManager = new ActiveDescendantKeyManager(this.options).withWrap();
  }
  public isOptions$(): Observable<boolean> {
    this.options.changes.pipe(untilDestroyed(this)).subscribe(changes => {
      this.isOptions.next(changes.length > 0);
    });
    return this.isOptions.asObservable();
  }

  public keyBoardValue$(): Observable<any> {
    return this._keyBoardSelectedValue$.asObservable();
  }

  optionsValues() {
    return this.options.changes.pipe(
      untilDestroyed(this),
      switchMap(options => {
        const values$ = options.map(option => option.value$);
        return combineLatest([...values$]);
      })
    );
  }
  optionsClick() {
    return this.options.changes.pipe(
      untilDestroyed(this),
      switchMap(options => {
        const clicks$ = options.map(option => option.click$);
        return merge(...clicks$);
      })
    );
  }
  ngOnDestroy(): void {
    this._keyBoardSelectedValue$.complete();
  }
}
