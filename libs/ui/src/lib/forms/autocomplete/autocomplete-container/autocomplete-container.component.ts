import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'rosa-autocomplete-container',
  templateUrl: './autocomplete-container.component.html',
  styleUrls: ['./autocomplete-container.component.scss']
})
export class AutocompleteContainerComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
