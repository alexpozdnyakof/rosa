import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { AutocompleteContainerComponent } from './autocomplete-container/autocomplete-container.component';
import { AutocompleteDirective } from './autocomplete.directive';
import { AutocompleteContentDirective } from './autocomplete-content.directive';
import { OptionComponent } from '../options/option/option.component';
import { SharedFormModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    AutocompleteComponent,
    AutocompleteContainerComponent,
    AutocompleteDirective,
    AutocompleteContentDirective,
    OptionComponent
  ],
  imports: [CommonModule, SharedFormModule],
  exports: [
    AutocompleteComponent,
    AutocompleteContainerComponent,
    AutocompleteDirective,
    AutocompleteContentDirective,
    OptionComponent
  ]
})
export class AutocompleteModule {}
