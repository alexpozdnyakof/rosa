import { Component, OnInit, Input, ElementRef, HostBinding, ViewEncapsulation, HostListener } from '@angular/core';
import { Observable, fromEvent, of } from 'rxjs';
import { mapTo, tap } from 'rxjs/operators';
@Component({
  selector: 'rosa-option',
  templateUrl: './option.component.html',
  styleUrls: ['./option.component.css'],
  encapsulation: ViewEncapsulation.None

})
export class OptionComponent implements OnInit {
  @Input() value: string;
  public click$: Observable<string>;
  public value$: Observable<string>;
  private _isActive = false;

  constructor(private el: ElementRef) { }

  @HostListener('mouseenter', ['$event']) onEnter( e: MouseEvent ) {
    this.setActiveStyles()
  }
  @HostListener('mouseleave', ['$event']) onLeave( e: MouseEvent ) {
    this.setInactiveStyles()
  }
  @HostBinding('class.is-focused') get isActive() {
    return this._isActive;
  };
  @HostBinding('class.host-option')
  get hostClass() {
    return true;
  };
  getLabel(): string {
    return this.value
  }
  setActiveStyles() {
    this._isActive = true;
  };

  setInactiveStyles() {
    this._isActive = false;
  }
  ngOnInit() {
    this.value$ = of(this.value);
    this.click$ = fromEvent(this.element, 'click').pipe(mapTo(this.value));
  }

  get element() {  return this.el.nativeElement; }

}
