import { NgModule } from '@angular/core';

import { ControlHelperComponent } from './control-helper/control-helper.component';
import { ControlErrorComponent } from './control-error/control-error.component';


@NgModule({
  declarations: [
    ControlHelperComponent,
    ControlErrorComponent
  ],
  exports: [
    ControlHelperComponent,
    ControlErrorComponent
  ]
})
export class CommonFormModule {}
