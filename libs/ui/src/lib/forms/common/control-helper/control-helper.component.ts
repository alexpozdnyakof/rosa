import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'rosa-control-helper',
  template: '<span class="rosaControlHelper"><ng-content></ng-content></span>',
  styles: ['.rosaControlHelper {font-size:12px;line-height:17px;color:#807d7d;font-weight:400;}'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ControlHelperComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
