import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlHelperComponent } from './control-helper.component';

describe('ControlHelperComponent', () => {
  let component: ControlHelperComponent;
  let fixture: ComponentFixture<ControlHelperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlHelperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlHelperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
