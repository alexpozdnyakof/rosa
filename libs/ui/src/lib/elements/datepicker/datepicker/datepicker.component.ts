import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter,
  Renderer2
} from '@angular/core';
import { Day } from './../day.interface';

@Component({
  selector: 'rosa-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.css']
})
export class DatepickerComponent implements OnInit {
  @Input() cursor = new Date();
  @Output() selectedDate: EventEmitter<string> = new EventEmitter();
  @ViewChild('monthPicker', { static: true }) monthPicker: ElementRef;
  public monthMode = false;
  public WEEK_DAYS = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
  public MONTH_LABELS = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
  ];
  public startDate = {
    month: new Date().getMonth(),
    year: new Date().getFullYear()
  };
  public currentDate = {
    month: this.startDate.month,
    year: this.startDate.year
  };
  public days: Day[] = [];
  public monthLabel: string;
  public year: string;
  constructor(private _renderer: Renderer2) {}

  public selectDay(day) {
    this.cursor = new Date(day);
    this.days = this.monthBuilder(
      this.currentDate.year,
      this.currentDate.month
    );
    this.selectedDate.emit(day);
  }
  public toggleMonthMode() {
    if (
      this.monthPicker.nativeElement.classList.contains(
        'rosa-datepicker__monthpicker_open'
      )
    ) {
      return this._renderer.removeClass(
        this.monthPicker.nativeElement,
        'rosa-datepicker__monthpicker_open'
      );
    }
    return this._renderer.addClass(
      this.monthPicker.nativeElement,
      'rosa-datepicker__monthpicker_open'
    );
  }
  ngOnInit() {
    this.days = this.monthBuilder(this.startDate.year, this.startDate.month);
  }

  public monthBuilder(year, month): Day[] {
    const firstDay = new Date(year, month, 1).getDay();
    const lastDay = new Date(
      year,
      month,
      this.getLastDayOfMonth(year, month)
    ).getDay();

    const offsetPrev = this.calculatePrevOffset(firstDay);
    const offsetNext = this.calculateNextOffset(lastDay);
    const days = this.makeMonth(year, month);
    const prevDays = this.makePrevMonth(year, month - 1, offsetPrev);
    const nextDays = this.makeNextMonth(year, month + 1, offsetNext);
    days.unshift(...prevDays);
    days.push(...nextDays);
    this.year = `${year}`;
    this.monthLabel = `${this.MONTH_LABELS[month]}`;
    return days;
  }

  public makeMonth(year: number, month: number): Day[] {
    const lastDay = this.getLastDayOfMonth(year, month);
    const days: Day[] = [];
    for (let i = 1; i <= lastDay; i++) {
      const date = new Date(year, month, i);
      days.push({
        date: date,
        isToday: this.isToday(date),
        isAvailable: true,
        isWeekend: this.isWeekend(date),
        isSelected: this.isSelected(date)
      });
    }
    return days;
  }
  public makePrevMonth(year, month, offset): Day[] {
    const prevMonthLastDay = this.getLastDayOfMonth(year, month);
    const days: Day[] = [];
    const offsetLimit = prevMonthLastDay - offset;
    for (let i = prevMonthLastDay; i > offsetLimit; i--) {
      const date = new Date(year, month, i);
      days.push({
        date: date,
        isToday: false,
        isAvailable: false,
        isWeekend: this.isWeekend(date),
        isSelected: this.isSelected(date)
      });
    }
    return days.reverse();
  }
  public makeNextMonth(year, month, offset): Day[] {
    const days: Day[] = [];
    for (let i = 1; i <= offset; i++) {
      const date = new Date(year, month, i);
      days.push({
        date: date,
        isToday: false,
        isAvailable: false,
        isWeekend: this.isWeekend(date),
        isSelected: this.isSelected(date)
      });
    }
    return days;
  }
  public getLastDayOfMonth(year, month) {
    const date = new Date(year, month + 1, 0);
    return date.getDate();
  }

  public isWeekend(date): boolean {
    return date.getDay() === 0 || date.getDay() === 6;
  }
  public isToday(date): boolean {
    const today = new Date();
    today.setHours(0, 0, 0, 0);
    return Number(date) === Number(today);
  }
  public isSelected(date): boolean {
    return Number(date) === Number(this.cursor.setHours(0, 0, 0, 0));
  }
  public changeMonth(direction: 'prev' | 'next') {
    // check if next month in another year
    if (direction === 'prev') {
      this.prevMonth();
    } else {
      this.nextMonth();
    }
    this.days = this.monthBuilder(
      this.currentDate.year,
      this.currentDate.month
    );
  }

  public setMonth(month: number) {
    this.currentDate.month = month;
    this.days = this.monthBuilder(
      this.currentDate.year,
      this.currentDate.month
    );
    this.toggleMonthMode();
  }

  public calculatePrevOffset(day) {
    return day === 0 ? 6 : day - 1;
  }
  public calculateNextOffset(day) {
    return day === 0 ? 0 : 7 - day;
  }
  private nextMonth() {
    if (this.currentDate.month === 11) {
      this.currentDate.month = 0;
      this.currentDate.year += 1;
    } else {
      this.currentDate.month += 1;
    }
  }
  private prevMonth() {
    if (this.currentDate.month === 0) {
      this.currentDate.month = 11;
      this.currentDate.year -= 1;
    } else {
      this.currentDate.month -= 1;
    }
  }
  public isCurrentMonth(index) {
    return index === this.currentDate.month;
  }
}

// текущий день
// текущий месяц
// текущий год
// последний день месяца
// создать массив из первый день - последний день
// определить каким днем недели является первый день
// определить каким днем недели является последний день
//  выполнить смещение относительно первого дня
// выполниить смещение относительно последнего дня
