import { storiesOf, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { DayComponent } from '../day/day.component';
import { DatepickerComponent } from './datepicker.component';
import { DatepickerNextComponent } from '../datepicker-next/datepicker-next.component';
import { DatepickerPrevComponent } from '../datepicker-prev/datepicker-prev.component';

storiesOf('DatePicker', module)
  .addDecorator(
    moduleMetadata({
      declarations: [
        DatepickerComponent,
        DayComponent,
        DatepickerNextComponent,
        DatepickerPrevComponent
      ],
      imports: [CommonModule]
    })
  )
  .add('default', () => {
    return {
      template: `
        <rosa-datepicker></rosa-datepicker>
      `
    };
  });
