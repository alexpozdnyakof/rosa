import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatepickerComponent } from './datepicker.component';
import { DayComponent } from '../day/day.component';
import { DatepickerNextComponent } from '../datepicker-next/datepicker-next.component';
import { DatepickerPrevComponent } from '../datepicker-prev/datepicker-prev.component';
import { doClassesMatch } from '../../../testing/do-classes-match';
import { By } from '@angular/platform-browser';

describe('DatepickerComponent', () => {
  let component: DatepickerComponent;
  let fixture: ComponentFixture<DatepickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DatepickerComponent,
        DayComponent,
        DatepickerNextComponent,
        DatepickerPrevComponent
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatepickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should set month name correctly', () => {
    console.log(component.monthLabel);
    component.currentDate = {
      month: 8,
      year: 2020
    };
    component.monthBuilder(
      component.currentDate.year,
      component.currentDate.month
    );
    fixture.detectChanges();
    expect(component.monthLabel).toBe('Сентябрь');
  });

  it('should toggle open class to monthPicker', () => {
    component.toggleMonthMode();
    fixture.detectChanges();
    expect(
      doClassesMatch(component.monthPicker.nativeElement.classList, [
        'rosa-datepicker__monthpicker_open'
      ])
    ).toBeTruthy();
    component.toggleMonthMode();
    fixture.detectChanges();
    expect(
      doClassesMatch(component.monthPicker.nativeElement.classList, [
        'rosa-datepicker__monthpicker_open'
      ])
    ).toBeFalsy();
  });
  it('should ++ curent month after clicking on next', () => {
    component.currentDate.month = 8;
    const currMonth = component.currentDate.month;
    const nextButton = fixture.debugElement.query(
      By.directive(DatepickerNextComponent)
    );
    nextButton.nativeElement.click();
    fixture.detectChanges();
    expect(component.currentDate.month).toBe(currMonth + 1);
  });
  it('should -- curent month after clicking on next', () => {
    component.currentDate.month = 8;
    const currMonth = component.currentDate.month;
    const pervButton = fixture.debugElement.query(
      By.directive(DatepickerPrevComponent)
    );
    pervButton.nativeElement.click();
    fixture.detectChanges();
    expect(component.currentDate.month).toBe(currMonth - 1);
  });
});
