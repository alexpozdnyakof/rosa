import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatepickerNextComponent } from './datepicker-next.component';

xdescribe('DatepickerNextComponent', () => {
  let component: DatepickerNextComponent;
  let fixture: ComponentFixture<DatepickerNextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DatepickerNextComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatepickerNextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
