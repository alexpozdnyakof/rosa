import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'rosa-datepicker-next',
  templateUrl: './datepicker-next.component.html',
  styleUrls: ['./datepicker-next.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatepickerNextComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
