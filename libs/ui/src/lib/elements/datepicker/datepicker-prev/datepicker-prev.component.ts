import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'rosa-datepicker-prev',
  templateUrl: './datepicker-prev.component.html',
  styleUrls: ['./datepicker-prev.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatepickerPrevComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
