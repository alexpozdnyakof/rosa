import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatepickerPrevComponent } from './datepicker-prev.component';

xdescribe('DatepickerPrevComponent', () => {
  let component: DatepickerPrevComponent;
  let fixture: ComponentFixture<DatepickerPrevComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DatepickerPrevComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatepickerPrevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
