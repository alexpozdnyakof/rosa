import {
  Component,
  OnInit,
  Input,
  ElementRef,
  Renderer2,
  Output,
  EventEmitter
} from '@angular/core';
import { Day } from './../day.interface';

@Component({
  selector: 'rosa-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.css']
})
export class DayComponent implements OnInit {
  @Input() day: Day;
  @Output() selected: EventEmitter<any> = new EventEmitter();
  constructor(private el: ElementRef, private renderer: Renderer2) {}
  emitDay() {
    this.selected.emit(this.day.date);
  }
  ngOnInit() {}
}
