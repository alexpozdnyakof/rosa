export interface Day {
  date?: Date;
  isToday?: boolean;
  isAvailable?: boolean;
  isWeekend?: boolean;
  isSelected?: boolean;
}
