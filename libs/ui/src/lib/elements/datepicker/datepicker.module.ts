import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DayComponent } from './day/day.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { DatepickerPrevComponent } from './datepicker-prev/datepicker-prev.component';
import { DatepickerNextComponent } from './datepicker-next/datepicker-next.component';

@NgModule({
  declarations: [
    DayComponent,
    DatepickerComponent,
    DatepickerPrevComponent,
    DatepickerNextComponent
  ],
  imports: [CommonModule],
  exports: [
    DayComponent,
    DatepickerComponent,
    DatepickerPrevComponent,
    DatepickerNextComponent
  ],
  entryComponents: [
    DayComponent,
    DatepickerComponent,
    DatepickerPrevComponent,
    DatepickerNextComponent
  ]
})
export class DatepickerModule {}
