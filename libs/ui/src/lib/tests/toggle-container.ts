import { TestBed, async } from '@angular/core/testing';
import { FormsModule, NgControl, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { CommonFormModule } from '../forms/common/common.module';


export function TemplateDrivenSpec(testContainer, testControl, testComponent, wrapperClass): void {
    fullSpec('template-driven', testContainer, testControl, testComponent, wrapperClass);
  }

  export function ReactiveSpec(testContainer, testControl, testComponent, wrapperClass): void {
    fullSpec('reactive', testContainer, testControl, testComponent, wrapperClass);
  }

function fullSpec(description, testContainer, directives: any | any[], testComponent, wrapperClass) {
    describe(description, () => {
      let fixture, containerDE, container, containerEl;
      if (!Array.isArray(directives)) {
        directives = [directives];
      }

      beforeEach(() => {
        TestBed.configureTestingModule({
          imports: [FormsModule, CommonFormModule, ReactiveFormsModule],
          declarations: [testContainer, ...directives, testComponent],
          providers: [NgControl],
        });
        fixture = TestBed.createComponent(testComponent);
        containerDE = fixture.debugElement.query(By.directive(testContainer));
        container = containerDE.componentInstance;
        containerEl = containerDE.nativeElement;
        fixture.detectChanges();
      });

      it('projects the label as last child', () => {
        const label = containerEl.querySelector('label');
        expect(label).toBeTruthy();
      });

      it('projects the control inside of the wrapper', () => {
        expect(containerEl.querySelector(wrapperClass)).toBeTruthy();
      });

      it('projects the helper text when the control is valid', () => {
        expect(containerEl.querySelector('rosa-control-helper')).toBeTruthy();
      });

      it("doesn't display the helper text when invalid", () => {
        expect(containerEl.querySelector('rosa-control-helper')).toBeTruthy();
        container.valid = false;
        fixture.detectChanges();
        expect(containerEl.querySelector('rosa-control-helper')).toBeFalsy();
      });

      it('projects the error helper when invalid', () => {
        expect(containerEl.querySelector('rosa-control-error')).toBeFalsy();
        container.valid = false;
        fixture.detectChanges();
        expect(containerEl.querySelector('rosa-control-error')).toBeTruthy();
      });
      /*
      it('sets error classes and displays the icon when invalid', () => {
        expect(containerEl.querySelector('.clr-control-container').classList.contains('clr-error')).toBeFalsy();
        expect(containerEl.querySelector('.clr-validate-icon')).toBeFalsy();
        container.invalid = true;
        fixture.detectChanges();
        expect(containerEl.querySelector('.clr-control-container').classList.contains('clr-error')).toBeTruthy();
        expect(containerEl.querySelector('.clr-validate-icon')).toBeTruthy();
      });*/
      /*

      */
      /*
      it('adds the .clr-form-control class to the host', () => {
        expect(containerEl.classList).toContain('clr-form-control');
      });
      */
      /*
      it('adds the error class for the control container', () => {
        expect(container.controlClass()).not.toContain('clr-error');
        container.invalid = true;
        expect(container.controlClass()).toContain('clr-error');
      });*/
      /*
      it('tracks the validity of the form control', () => {
        expect(container.invalid).toBeFalsy();
        markControlService.markAsTouched();
        fixture.detectChanges();
        expect(container.invalid).toBeTruthy();
      });
      */
      /*
      it('tracks the disabled state', async(() => {
        const test = fixture.debugElement.componentInstance;
        test.disabled = true;
        fixture.detectChanges();
        // Have to wait for the whole control to settle or it doesn't track
        fixture.whenStable().then(() => {
          expect(containerEl.className).not.toContain('clr-form-control-disabled');
          if (test.form) {
            // Handle setting disabled based on reactive form
            test.form.get('model').reset({ value: '', disabled: true });
          }
          fixture.detectChanges();
          expect(containerEl.className).toContain('clr-form-control-disabled');
        });
      }));*/
      it('implements ngOnDestroy', () => {
        expect(container.ngOnDestroy).toBeDefined();
      });
    });
  }
