import { TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';


export function ControlStandaloneSpec(testComponent): void {
    describe('standalone use', () => {
      it('should not throw an error when used without a form control', () => {
        TestBed.configureTestingModule({
          imports: [],
          declarations: [testComponent],
        });
        expect(() => {
          const fixture = TestBed.createComponent(testComponent);
          fixture.detectChanges();
        }).not.toThrow();
      });
    });
  }