import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonDirective } from './buttons/button/button.directive';
import { RosaFormModule } from './forms/forms.module';
import { LayoutModule } from './layout/layout.module';
import { ContentModule } from './content/content.module';
import { DatepickerModule } from './elements/datepicker/datepicker.module';

@NgModule({
  imports: [
    CommonModule,
    RosaFormModule,
    LayoutModule,
    ContentModule,
    DatepickerModule
  ],
  declarations: [ButtonDirective],
  exports: [RosaFormModule, LayoutModule, ContentModule, DatepickerModule]
})
export class UiModule {}
