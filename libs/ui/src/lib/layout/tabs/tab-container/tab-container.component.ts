import { Component, OnInit, EventEmitter, ContentChildren, QueryList, AfterViewInit, AfterContentInit, Input, Output, ViewChild, ElementRef, Renderer2, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { TabComponent } from '../tab/tab.component';
import { merge } from 'rxjs';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'rosa-tab-container',
  templateUrl: './tab-container.component.html',
  styleUrls: ['./tab-container.component.css']
})
export class TabContainerComponent implements OnInit, AfterViewInit, OnChanges, AfterContentInit, OnDestroy {
  @ContentChildren(TabComponent)
  tabs: QueryList<TabComponent>;
  @Input() active: string;
  @Output() selected: EventEmitter<string> = new EventEmitter();
  @ViewChild('tabUnderline',{static:true})
  tabUnderline: ElementRef;

  constructor(
    private _renderer: Renderer2
  ) { }
  ngOnChanges(changes: SimpleChanges){
    if(changes.active && this.tabs){
        this.setActive()
     }
  }
  ngAfterContentInit(){
    this.setActive();
    const clicks$ = this.tabs.map(tab => tab.click$);
    const mergeClicks = merge(...clicks$);
    mergeClicks
    .pipe(untilDestroyed(this))
    .subscribe(
      value => {
       this.active = value;
       this.selected.emit(this.active);
       this.setActive();
      }
    )
  }

  setActive(){
    this.tabs.map((tab: TabComponent) => tab._active = false);
    if(this.active){
      const tabActive = this.tabs.filter((tab: TabComponent) => tab.value === this.active);
      tabActive[0]._active = true;
      this._renderer.setStyle(this.tabUnderline.nativeElement, 'width', `${tabActive[0].width}px`)
      this._renderer.setStyle(this.tabUnderline.nativeElement, 'transform', ` matrix(1, 0, 0, 1, ${tabActive[0].offsetLeft}, -2)`)
      return;
    }
    const tabs = this.tabs.map((tab: TabComponent) => tab);
    tabs[0]._active = true;
    this._renderer.setStyle(this.tabUnderline.nativeElement, 'width', `${tabs[0].width}px`)
    this._renderer.setStyle(this.tabUnderline.nativeElement, 'transform', ` matrix(1, 0, 0, 1, ${tabs[0].offsetLeft}, -2)`)
  }
  ngAfterViewInit(){}
  ngOnInit(){}
  ngOnDestroy(){}
}
