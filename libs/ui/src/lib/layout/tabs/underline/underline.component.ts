import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'rosa-underline',
  template: '<div class="rosa-underline"></div>',
  styles: [':host{display: block} .rosa-underline{width: 100%; height: 4px; background-color:#e60028}'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UnderlineComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
