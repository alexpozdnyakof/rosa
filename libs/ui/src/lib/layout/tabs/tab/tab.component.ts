import { Component, OnInit, Input, Renderer2, ChangeDetectionStrategy, OnChanges, SimpleChanges, ElementRef, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Observable, fromEvent } from 'rxjs';
import { mapTo } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'rosa-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TabComponent implements OnInit, OnDestroy {

  @Input() active = false;
  @Input() value: string;
  public click$: Observable<string>;
  public width;
  public offsetLeft;
  constructor(
    private renderer: Renderer2,
    private el: ElementRef,
    public cdr: ChangeDetectorRef)
  { }
  set _active(value){
    this.active = value;
    if(this.active){
      this.renderer.addClass(this.el.nativeElement, 'rosa-tab__active');
    } else {
      this.renderer.removeClass(this.el.nativeElement, 'rosa-tab__active');
    }
    this.cdr.detectChanges();
  }
  ngOnInit() {
    this.width = this.el.nativeElement.offsetWidth;
    this.offsetLeft = this.el.nativeElement.offsetLeft;
    this.click$ = fromEvent(this.el.nativeElement, 'click').pipe(untilDestroyed(this), mapTo(this.value));
  }
  ngOnDestroy(){}
}
