import { Component, OnInit, Input, ChangeDetectionStrategy, Renderer2, ElementRef, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Observable, fromEvent } from 'rxjs';
import { mapTo } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'rosa-step',
  templateUrl: './step.component.html',
  styleUrls: ['./step.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StepComponent implements OnInit, OnDestroy {
  @Input() active = false;
  @Input() value: string;
  @Input() index: string;
  public click$: Observable<string>;
  public offsetLeft;

  constructor(
    private renderer: Renderer2,
    private el: ElementRef,
    public cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.offsetLeft = this.el.nativeElement.offsetLeft;
    this.click$ = fromEvent(this.el.nativeElement, 'click').pipe(
      untilDestroyed(this),
      mapTo(this.value));
  }

  set _active(value){
    this.active = value;
    if(this.active){
      this.renderer.addClass(this.el.nativeElement, 'step_active');
    } else {
      this.renderer.removeClass(this.el.nativeElement, 'step_active');
    }
    this.cdr.detectChanges();
  }
  ngOnDestroy(){}

}
