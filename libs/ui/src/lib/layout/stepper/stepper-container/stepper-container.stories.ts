
import { storiesOf, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { StepperContainerComponent } from './stepper-container.component';
import { StepComponent } from '../step/step.component';
import { StepProgressComponent } from '../step-progress/step-progress.component';

storiesOf('Stepper', module)
  .addDecorator(
    moduleMetadata({
      declarations: [
        StepperContainerComponent,
        StepComponent,
        StepProgressComponent
      ],
      imports: [
        CommonModule,
      ],
    })
  )
  .add('default', () => {
    return {
      template: `
      <section class="rosa_form-group" style="width:100%; padding-top: 15px; width: 100%;height: 90px;box-shadow: 0 -1px 12px rgba(171, 172, 176, 0.3);">
        <rosa-stepper-container [active]="'account'">
            <rosa-step [index]="'1'" [value]="'info'"></rosa-step>
            <rosa-step [index]="'2'" [value]="'account'"></rosa-step>
            <rosa-step [index]="'3'" [value]="'approve'"></rosa-step>
            <rosa-step-progress></rosa-step-progress>
        </rosa-stepper-container>
      </section>
      `,
    };
  })
  .add('clickable', () => {
    return {
      template: `
      <section class="rosa_form-group" style="width:100%; padding-top: 15px; width: 100%;height: 90px;box-shadow: 0 -1px 12px rgba(171, 172, 176, 0.3);">
        <rosa-stepper-container [clickable]="true">
            <rosa-step [index]="'1'" [value]="'info'"></rosa-step>
            <rosa-step [index]="'2'" [value]="'account'"></rosa-step>
            <rosa-step [index]="'3'" [value]="'approve'"></rosa-step>
            <rosa-step-progress></rosa-step-progress>
        </rosa-stepper-container>
      </section>
      `,
    };
  })
