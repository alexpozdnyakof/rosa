import {
  Component,
  OnInit,
  AfterContentInit,
  ContentChildren,
  QueryList,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  Renderer2,
  OnDestroy,
  HostBinding
} from '@angular/core';
import { StepComponent } from '../step/step.component';
import { merge } from 'rxjs';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'rosa-stepper-container',
  templateUrl: './stepper-container.component.html',
  styleUrls: ['./stepper-container.component.css']
})
export class StepperContainerComponent
  implements OnInit, AfterContentInit, OnDestroy {
  @ContentChildren(StepComponent)
  steps: QueryList<StepComponent>;
  @Input() active: string;
  @Input() public clickable = false;
  @Output() selected: EventEmitter<any> = new EventEmitter();
  @ViewChild('progressLine', { static: true })
  progressLine: ElementRef;

  constructor(private el: ElementRef, private _renderer: Renderer2) {}

  ngAfterContentInit() {
    this.setActive();
    if (this.clickable) {
      const clicks$ = this.steps.map(step => step.click$);
      const mergeClicks = merge(...clicks$);
      mergeClicks.pipe(untilDestroyed(this)).subscribe(value => {
        this.active = value;
        this.selected.emit(this.active);
        this.setActive();
      });
    }
  }

  setActive() {
    this.steps.map((step: StepComponent) => (step._active = false));
    if (this.active) {
      const stepActive = this.steps.filter(
        (step: StepComponent) => step.value === this.active
      );
      const activeIndex = this.steps.toArray().indexOf(stepActive[0]);
      stepActive[0]._active = true;
      this.steps.forEach((currentValue, index, array) => {
        return index < activeIndex ? (currentValue._active = true) : null;
      });
      this._renderer.setStyle(
        this.progressLine.nativeElement,
        'width',
        `${(stepActive[0].offsetLeft * 100) /
          this.el.nativeElement.clientWidth}%`
      );
      return;
    }
    const steps = this.steps.map((step: StepComponent) => step);
    steps[0]._active = true;
    this._renderer.setStyle(
      this.progressLine.nativeElement,
      'width',
      `${(steps[0].offsetLeft * 100) / this.el.nativeElement.clientWidth}%`
    );
  }
  ngOnInit() {}
  ngOnDestroy() {}
}
