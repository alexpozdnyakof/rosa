import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepperContainerComponent } from './stepper-container.component';
import { Component } from '@angular/core';
import { StepComponent } from '../step/step.component';
import { By } from '@angular/platform-browser';

@Component({
  template: `
    <rosa-stepper-container [active]="'account'">
      <rosa-step [index]="'1'" [value]="'info'"></rosa-step>
      <rosa-step [index]="'2'" [value]="'account'"></rosa-step>
      <rosa-step [index]="'3'" [value]="'approve'"></rosa-step>
    </rosa-stepper-container>
  `
})
class SimpleTest {}

describe('StepperContainerComponent', () => {
  let fixture: ComponentFixture<SimpleTest>;
  let containerDE, container, containerEl, step;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StepperContainerComponent, StepComponent, SimpleTest]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleTest);
    step = fixture.debugElement.query(By.directive(StepComponent));
    containerDE = fixture.debugElement.query(
      By.directive(StepperContainerComponent)
    );
    container = containerDE.componentInstance;
    containerEl = containerDE.nativeElement;
    fixture.detectChanges();
  });

  it('projects the step inside of the wrapper', () => {
    expect(containerEl.querySelector('rosa-step')).toBeTruthy();
  });
  it('projects the all steps inside of the wrapper', () => {
    expect(containerEl.querySelectorAll('rosa-step').length).toBe(3);
  });
  it('should set active value', () => {
    expect(container.active).toBe('account');
  });
  it('projects should have progressLIne', () => {
    expect(container.progressLine).toBeDefined();
  });
  it('should set first step active when container value not setted', () => {
    container.active = null;
    fixture.detectChanges();
    const tabActive = container.steps.first;
    expect(tabActive.active).toBeTruthy();
  });

  it('should not set new value when clicking if clickable false', () => {
    container.clickable = false;
    container.ngAfterContentInit();
    const steps = container.steps.map(
      (tabInContainer: StepComponent) => tabInContainer
    );
    const stepsEl = containerEl.querySelectorAll('rosa-step');
    stepsEl[2].click();
    fixture.detectChanges();
    expect(container.active).toBe(steps[1].value);
    expect(steps[0].active).toBeTruthy();
    expect(steps[1].active).toBeTruthy();
    expect(steps[2].active).toBeFalsy();
  });

  it('should set new value when clicking if clickable true', () => {
    const steps = container.steps.map(
      (stepInContainer: StepComponent) => stepInContainer
    );
    const stepsEl = containerEl.querySelectorAll('rosa-step');
    container.clickable = true;
    container.ngAfterContentInit();
    stepsEl[2].click();
    fixture.detectChanges();
    //expect(container.active).toBe(steps[2].value);
    expect(steps[0].active).toBeTruthy();
    expect(steps[1].active).toBeTruthy();
    expect(steps[2].active).toBeTruthy();
  });
  it('implements ngOnDestroy', () => {
    expect(container.ngOnDestroy).toBeDefined();
  });
});
