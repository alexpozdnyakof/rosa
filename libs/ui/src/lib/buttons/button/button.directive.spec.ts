import { ButtonDirective } from './button.directive';
import { TestBed, ComponentFixture, TestModuleMetadata } from '@angular/core/testing';
import { ElementRef, Renderer2, Component, Injectable, inject, DebugElement } from '@angular/core';
import { getElement } from '../../testing/get-element';
import { doClassesMatch } from '../../testing/do-classes-match';
declare var document;
@Component({
  template: `
      <button rosaButton>Button</button>
    `
})
class TestComponent { }

describe('Directive: ButtonDirective', () => {
  let fixture: ComponentFixture<any>;
  beforeEach(() => {
    const testModuleMetadata: TestModuleMetadata = {
      declarations: [ButtonDirective, TestComponent],
    };
    fixture = TestBed.configureTestingModule(testModuleMetadata).createComponent(TestComponent);
    fixture.detectChanges();
  })
  describe('Button: without params', () => {
    let buttonElement = null;
    beforeEach(() => {
      const defaultTrueElementIndex = 0;
      buttonElement = getElement(fixture, defaultTrueElementIndex);
    });
    it('should primary class without params', () => {
      expect(doClassesMatch(buttonElement.classList, ['rosa-button_primary'])).toBeTruthy();
    });
  })

  afterEach(() => { fixture = null; })
});
