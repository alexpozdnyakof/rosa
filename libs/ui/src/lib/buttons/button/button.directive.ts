import { Directive, ElementRef, Renderer2, OnInit, Input } from '@angular/core';

@Directive({
  selector: 'button[rosaButton], a[rosaButton]'
})
export class ButtonDirective implements OnInit {
  private _type: 'primary' | 'secondary' | 'tertiary' = 'primary';
  public text: string;
  private _state: 'active' | 'loading' | 'complete' | 'default' = `default`;
  constructor(
    private el: ElementRef,
    private renderer :Renderer2
  ) { }
  @Input('rosaButton')
  set setType(value: 'primary' | 'secondary' | 'tertiary') {
    this._type = value || 'primary';
  }

  public set setText(value) {
    this.text = value;
    this.rewriteHTML();
  }

  set setState(state: 'active' | 'loading' | 'complete' | 'default') {
    this._state = state || `default`;
    this.chooseState();
  }

  public ngOnInit(): void {
    const type = `rosa-button_${this._type}`;
    this.renderer.addClass(this.el.nativeElement, type);
    this.text = this.el.nativeElement.innerHTML; // сохраним текущее содержимое кнопки чтобы не потерять при очистке
    this.rewriteHTML();
  }

  public chooseState(){
    const state = `rosa-button_${this._state}`;
    this.renderer.addClass(this.el.nativeElement, state); // установим класс стейта
    switch(this._state){ // обработаем частные случаи
      case `loading`:
        this.el.nativeElement.disabled = true;
        break;
      case `default`:
         this.renderer.removeClass(this.el.nativeElement, 'rosa-button_active');
         break;
    }
  }
  private rewriteHTML(){
    const html = this.createTemplate();
    this.renderer.setProperty(this.el.nativeElement, 'innerHTML', '');
    this.renderer.setProperty(this.el.nativeElement, 'innerHTML', html);
  }
  private createTemplate(){
    return `
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" class="rosa-button__complete-icon"><path fill-rule="evenodd" clip-rule="evenodd" d="M21.744 5.67L10.23 18.44l-6.926-6.722 1.393-1.435 5.437 5.277L20.258 4.33l1.486 1.34z" fill="#FFF"></svg>
    <div class="rosa-button__spinner"></div>
    <span class="rosa-button__overlay"></span>
    <span class="rosa-button__text">${this.text}</span>
  `;
  }
}
