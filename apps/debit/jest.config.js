module.exports = {
  name: 'debit',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/debit',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
