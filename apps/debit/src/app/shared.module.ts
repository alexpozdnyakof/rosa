import { NgModule } from '@angular/core';
import { UiModule } from '@rosa/ui';

@NgModule({
  declarations: [],
  imports: [
    UiModule,

  ],
  exports: [
    UiModule,
  ]
})
export class SharedModule { }
