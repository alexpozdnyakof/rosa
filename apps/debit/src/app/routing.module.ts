import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [

  {
    path: 'collection',
    loadChildren: () => import('./collection/collection.module').then(m => m.CollectionModule),
    // canLoad: [AuthGuard]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
